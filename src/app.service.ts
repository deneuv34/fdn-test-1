import { Injectable, HttpService } from '@nestjs/common';
import { Observable } from 'rxjs';
import axios from 'axios';
// tslint:disable-next-line:no-var-requires
const passport = require('passport');
// tslint:disable-next-line:no-var-requires
const FacebookStrategy = require('passport-facebook').Strategy;

@Injectable()
export class AppService {
    public token: string;

    constructor(
        private readonly httpService: HttpService,
    ) {}

    public getFacebookAccessToken() {
        passport.use(new FacebookStrategy({
            clientID: '2186758351562553',
            clientSecret: '7f74bda3c28126f1d75dbb6b8e175f4a',
            callbackURL: 'http://localhost:3000/facebook/callback',
          },
          (accessToken, refreshToken, profile, cb) => {
            this.token = accessToken;
            return cb(null, accessToken);
          },
        ));
    }

    public async getFeed(accessToken: string): Promise<any> {
        const response = await axios.get(`https://graph.facebook.com/me?fields=id,name,birthday,email,feed&access_token=${accessToken}`);
        return response.data;
    }
}
