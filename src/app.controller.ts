import { Controller, Get, Req, Post, Body } from '@nestjs/common';
import { AppService } from './app.service';

@Controller('facebook')
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get('/feed')
  async getFeed(@Req() request: any) {
    // return this.appService.getFacebookAccessToken();
    // tslint:disable-next-line:no-console
    return await this.appService.getFeed(request.query.token);
  }

  // @Get('/callback')
  @Post('/callback')
  returnCallback(@Body() request: any) {
    // return await this.appService.getFeed(request.token);
    return request;
  }
}
