import { Test, TestingModule } from '@nestjs/testing';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { HttpModule } from '@nestjs/common';

describe('AppController', () => {
  let appController: AppController;
  let appService: AppService;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      controllers: [AppController],
      providers: [AppService],
      imports: [HttpModule],
    }).compile();

    appService = app.get<AppService>(AppService);
    appController = app.get<AppController>(AppController);
  });

  describe('root', () => {
    it('appService should have been called', async () => {
      const result = {};
      jest.spyOn(appService, 'getFeed').mockImplementation(async () => result);
      await appController.getFeed({query : {token : 'asd'}});
      expect(await appService.getFeed).toHaveBeenCalled();
    });
  });
});
