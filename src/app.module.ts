import { Module, HttpModule } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';

@Module({
  imports: [HttpModule],
  providers: [AppService],
  controllers: [AppController],
})
export class AppModule {}
