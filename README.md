# Social plugin. FDN Test - 1

## Description

Plugin to get user feeds using facebook username and password.

## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Test

```bash
# unit tests
$ npm run test

# test coverage
$ npm run test:cov
```

# Get User Feed

Endpoint to get user's feed
```bash
GET http://localhost:3000/facebook/feed?token=<facebook_access_token>
```

# TODO

- First time to using Facebook Graph API, still not figured out how to get feed directly from Facebook OAuth callback
- OAuth request doesn't return anything or go to callback app url
- Now, The request need facebook `access_token` to get the feeds

# Author
Rangga Adhitya Prawira (deneuv3.4@gmail.com)

*Git profile*: [GitHub](https://github.com/deneuv34), [GitLab]('https://gitlab.com/deneuv34')